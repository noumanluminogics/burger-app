import React, { Component } from "react";

class BuildControl extends Component {
  render() {
    return (
      <div className="BuildControl">
        <div className="label">{this.props.label}</div>
        <button
          className="Less"
          onClick={this.props.removed}
          disabled={this.props.disabled}
        >
          Less
        </button>
        <button className="More" onClick={this.props.added}>
          More
        </button>
      </div>
    );
  }
}
export default BuildControl;
