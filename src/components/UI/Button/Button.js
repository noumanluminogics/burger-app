import React, { Component } from "react";

class Button extends Component {
  render() {
    const buttonClass = "Button " + this.props.btnType;

    console.log(buttonClass);
    return <button onClick={this.props.clicked} className="Button"></button>;
  }
}

export default Button;
